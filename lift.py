import collections
from generators import generate_passengers, NUM_FLOORS

passengers_by_second = generate_passengers(num_seconds=20)

# debug
# print(passengers_by_second)

# should initialize main queue here before loop
# queue by floor and note time?
# queue by second and note floor?

# Need some constants for how long it takes to move between floors

for second, passengers in passengers_by_second.items():
    print("processing second number {sec}".format(sec=second))
    for passenger in passengers:
        print(passenger)
        # Process passengers and queue
        # Move floors
        # Update passengers and queue

        # show queue

# Outputs - total time taken?
# Average ride time per user?
# Median ride time could be different
# Number of floors traveled? Low wear and tear could be a requirement
# Some other efficiency measurement?
