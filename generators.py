#!/usr/bin/env python3

import collections
import random

# Random id for unique name
from uuid import uuid4

NUM_FLOORS = 5

# This should be pretty easy to generate for X number of floors but hardcoding for now
FLOORS = {
    1: {
        "possible_destinations": [2,3,4,5],
        "destination_weights": [1,1,1,1],
    },
    # Twice as likely to go from upper floor to first floor
    2: {
        "possible_destinations": [1,3,4,5],
        "destination_weights": [2,1,1,1],
    },
    3: {
        "possible_destinations": [1,2,4,5],
        "destination_weights": [2,1,1,1],
    },
    4: {
        "possible_destinations": [1,2,3,5],
        "destination_weights": [2,1,1,1],
    },
    5: {
        "possible_destinations": [1,2,3,4],
        "destination_weights": [2,1,1,1],
    }
}

Passenger = collections.namedtuple("Person", "name source destination")

# Variable number of floors is not supported
# Returns a dict with each numbered second as the key and a list of passengers 
def generate_passengers(num_seconds=60): #, num_floors=5):
    queue_length_odds_all_floors = {
        0: 0.990,
        1: 0.005,
        2: 0.005 / 2,
        3: 0.005 / 4,
        4: 0.005 / 8,
        5: 0.005 / 16
    }

    passengers_by_time = dict.fromkeys(range(0, num_seconds), [])

    for second in passengers_by_time:
        queue = []
        for floor_number in FLOORS:
            # random.choices returns a list (length 1) so we have to get item[0] to actually get the queue number
            queue_length = random.choices(list(queue_length_odds_all_floors.keys()), weights=queue_length_odds_all_floors.values())[0]
            while queue_length > 0:
                destination_floor = random.choices(FLOORS[floor_number]["possible_destinations"], weights=FLOORS[floor_number]["destination_weights"])[0]
                passenger = Passenger(
                    name=uuid4().hex,
                    source=floor_number,
                    destination=destination_floor
                )
                queue.append(passenger)
                queue_length -= 1
        passengers_by_time[second] = queue

    return passengers_by_time