# Usage
python3 lift.py

# Status
Well, I wrote something that generates a random set of passengers by second for a given number of seconds.
This is meant to be used as an input to a simulation of an elevator's logic loop, assuming it's pretty simple and checks for new requests once per second.

I had enough fun writing the generator and probably won't pick this up for a while.